from django.apps import AppConfig


class Story4AppConfig(AppConfig):
    name = 'story4_app'
