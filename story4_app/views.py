from django.shortcuts import render

# Create your views here.
def riwayat_view(request):
	return render(request, "pages/riwayat.html")

def portofolio_view(request):
	return render(request, "pages/portofolio.html")

def logs_view(request):
	return render(request, "pages/logs.html")
