from django.urls import path
from . import views

app_name = 'story4_app'
urlpatterns = [
    path('riwayat', views.riwayat_view, name="riwayat_url"),
    path('portofolio', views.portofolio_view, name="portofolio_url"),
    path('logs', views.logs_view, name="logs_url"),
]