from django.urls import path
from . import views

app_name = 'story1_app'
urlpatterns = [
    path('', views.home_view, name="home_url"),
]