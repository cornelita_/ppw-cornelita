const hamburgerButton = document.querySelector("#hamburger");
const drawerElement = document.querySelector("#drawer");
const bodyElement = document.querySelector("body");

hamburgerButton.addEventListener("click", event => {
    drawerElement.classList.toggle("open");
    event.stopPropagation();
})

drawerElement.addEventListener("click", event => {
    drawerElement.classList.remove("open");
    event.stopPropagation();
})

bodyElement.addEventListener("click", event => {
    drawerElement.classList.remove("open");
    event.stopPropagation();
})