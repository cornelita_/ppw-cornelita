from django.urls import path, include
from . import views

app_name = 'story9_app'
urlpatterns = [
    path('surprise', views.showSurprise, name="surprise"),
    path('login', views.loginPage, name='login'),
    path('register', views.registerPage, name='register'),
    path('logout', views.logoutUser, name='logout'),
]