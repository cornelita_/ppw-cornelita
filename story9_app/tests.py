from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .views import loginPage, registerPage, logoutUser, showSurprise
from django.contrib.auth.models import User

# Create your tests here.
class Story9Test(TestCase):
    # Test function
    def test_surprise_url_using_showsurprise_func(self):
        found = resolve('/surprise')
        self.assertEqual(found.func, showSurprise)

    def test_login_url_using_loginpage_func(self):
        found = resolve('/login')
        self.assertEqual(found.func, loginPage)

    def test_register_url_using_registerpage_func(self):
        found = resolve('/register')
        self.assertEqual(found.func, registerPage)

    def test_logout_url_using_logoutuser_func(self):
        found = resolve('/logout')
        self.assertEqual(found.func, logoutUser)

    # Test URL
    def test_surprise_url_no_user_authenticated(self):
        response = Client().get('/surprise')
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertIn("Want to see something exciting?", html_contents)
        self.assertIn("Login", html_contents)
        self.assertNotIn("surpriseImage", html_contents)
        self.assertNotIn("Logout", html_contents)

    def test_surprise_url_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/surprise')
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertNotIn("Want to see something exciting?", html_contents)
        self.assertNotIn("Login", html_contents)
        self.assertIn("surpriseImage", html_contents)
        self.assertIn("Logout", html_contents)

    def test_register_url_wrong_data(self):
        response = Client().post('/register', data={
            "username": "testing",
            "password1": "maululus1112",
            "password2": "maululus"
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertIn('<div class="error-message">', html_contents)

    def test_register_url_correct_data(self):
        response = Client().post('/register', data={
            "username": "testing",
            "password1": "maululus1112",
            "password2": "maululus1112"
        })
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/login')
        counter = User.objects.all().count()
        self.assertEqual(counter, 1)

    def test_register_url_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/register')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/surprise')

    def test_login_url_wrong_data(self):
        response = Client().post('/login', data={
            "username": "testing",
            "password": "maululus",
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertIn('Username or password is incorrect', html_contents)

    def test_login_url_correct_data(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        response = Client().post('/login', data={
            "username": "testing",
            "password": "maululus1112",
        })
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/surprise')

    def test_login_url_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/login')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/surprise')


    def test_logout_url(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/logout')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/surprise')
