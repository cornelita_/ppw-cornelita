from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .views import showPage

# Create your tests here.
class Story7Test(TestCase):
    # Test URL
    def test_activity_url_is_exist(self):
        response = Client().get('/story7')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_activity_using_activity_func(self):
        found = resolve('/story7')
        self.assertEqual(found.func, showPage)
