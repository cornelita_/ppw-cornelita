from django.urls import path
from . import views

app_name = 'story7_app'
urlpatterns = [
    path('story7', views.showPage, name="story7"),
]