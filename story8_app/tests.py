from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .views import showBook, dataBook, process

# Create your tests here.
class Story8Test(TestCase):
    # Test URL
    def test_story8_url_is_exist(self):
        response = Client().get('/story8')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_story8_using_showbook_func(self):
        found = resolve('/story8')
        self.assertEqual(found.func, showBook)

    # Test get data from API
    def test_get_data_no_querry(self):
        response = Client().get('/data/', {'querryBook': ''})
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_get_data_with_querry(self):
        response = Client().get('/data/', {'querryBook': 'abcdefghijklmn'})
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_get_data_API_return_no_data(self):
        response = Client().get('/data/', {'querryBook': 'gritttttttttt'})
        self.assertEquals(response.status_code, HTTPStatus.OK)
        # Will be fixed later 😭
        # self.assertJSONEqual(
        #     str(response.content, encoding='utf8'),
        #     [{"error": "We're sorry, there is no book with that title"}]
        # )