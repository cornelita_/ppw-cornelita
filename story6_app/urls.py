from django.urls import path
from . import views

app_name = 'story6_app'
urlpatterns = [
    path('addPerson/<int:idActivity>', views.addPerson, name="add_person_url"),
    path('addActivity', views.addActivity, name="add_activity_url"),
    path('activity', views.my_activity, name="activity_url"),
]