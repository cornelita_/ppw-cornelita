from django.shortcuts import render, redirect
from django.core.exceptions import ValidationError
from .models import Activity, Person
from .forms import ActivityForm, PersonForm

# Create your views here.
def my_activity(request):
    formPerson = PersonForm()
    formActivity = ActivityForm()
    activities = Activity.objects.all()
    html = "pages/activity.html"
    return render(request, html, {'formPerson': formPerson, 'formActivity': formActivity, 'activities': activities})

def getPerson(name):
    person = Person.objects.filter(person_name = name)
    if person.exists():
        return person.first()
    else:
        return False

def addPerson(request, idActivity):
    formPerson = PersonForm(request.POST)
    activity = Activity.objects.filter(pk = idActivity)
    if (activity.exists()) :
        if formPerson.is_valid():
            person = formPerson.save()
            activity.first().list_person.add(person)
        else :
            person = getPerson(request.POST['person_name'])
            if person:
                activity.first().list_person.add(person)
    return redirect('story6_app:activity_url')

def addActivity(request):
    formActivity = ActivityForm(request.POST)
    if (formActivity.is_valid()):
        formActivity.save()
    return redirect('story6_app:activity_url')