from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .models import Activity, Person
from .views import addActivity, addPerson, my_activity

# Create your tests here.
class Story6Test(TestCase):
    # Test URL
    def test_activity_url_is_exist(self):
        response = Client().get('/activity')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_activity_using_activity_func(self):
        found = resolve('/activity')
        self.assertEqual(found.func, my_activity)

    def test_activity_html(self):
        response = Client().get('/activity')
        html_contents = response.content.decode('utf8')
        self.assertIn("Daftar Kegiatan Lita", html_contents)
        self.assertIn("Tambah Kegiatan", html_contents)

    def test_post_activity_form(self):
        response = self.client.post(
            "/addActivity", data={"activity_name" : "Menangis T-T"}
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_post_person_form_new_person(self):
        new_activity = Activity.objects.create(activity_name = 'Kegiatan 1')
        my_url = "/addPerson/" + str(new_activity.pk)
        response = self.client.post(
            my_url, data={"person_name" : "Cornelita"}
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_post_person_form_not_new_person(self):
        new_activity = Activity.objects.create(activity_name = 'Kegiatan 1')
        person_1 = Person.objects.create(person_name = 'Cornelita')
        my_url = "/addPerson/" + str(new_activity.pk)
        response = self.client.post(
            my_url, data={"person_name" : "Cornelita"}
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)

    def test_post_wrong_person_form(self):
        new_activity = Activity.objects.create(activity_name = 'Kegiatan 1')
        person_1 = Person.objects.create(person_name = 'Cornelita')
        my_url = "/addPerson/" + str(new_activity.pk)
        response = self.client.post(
            my_url, data={"person_name" : "Cornelita"*100}
        )
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        
    # Test Models
    def test_add_activity(self):
        new_activity = Activity.objects.create(activity_name = 'Kegiatan 1')
        jumlah = Activity.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_add_person(self):
        person_1 = Person.objects.create(person_name = 'Cornelita')
        jumlah = Person.objects.all().count()
        self.assertEqual(jumlah, 1)
