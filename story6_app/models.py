from django.db import models

# Create your models here.
class Person(models.Model):
    person_name = models.CharField(
        'Nama',
        max_length = 100, 
        help_text = 'Cornelita',
        unique = True
    )

class Activity(models.Model):
    activity_name = models.CharField(
        'Judul Kegiatan', 
        max_length = 100, 
        help_text = 'Menangis T-T'
    )
    list_person = models.ManyToManyField(Person)