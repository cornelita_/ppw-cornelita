from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
SEMESTER_CHOICES = ( 
    ("Gasal 2020/2021", "Gasal 2020/2021"),
    ("Genap 2019/2020", "Genap 2019/2020"), 
    ("Gasal 2019/2020", "Gasal 2019/2020"),
    ("Other", "Other"),
)

class MataKuliah(models.Model):
    nama_matkul = models.CharField(
        'Mata Kuliah', 
        max_length = 100, 
        help_text = 'PPW'
    )
    dosen_pengajar = models.CharField(
        'Dosen Pengajar', 
        max_length = 100, 
        help_text = 'Gladhi Guarddin S.Kom., M. Kom.'
    )
    jumlah_sks = models.IntegerField(
        'Jumlah SKS',
        default = 1,
        validators = [
            MinValueValidator(1),
            MaxValueValidator(24)
        ]
    )
    deskripsi = models.CharField(
        'Deskripsi',
        max_length = 500
    )
    semester_tahun = models.CharField(
        'Semester Tahun',
        max_length = 15,
        choices = SEMESTER_CHOICES,
        default = 'Gasal 2020/2021'
    )
    ruang_kelas = models.CharField(
        'Ruang Kelas', 
        max_length = 20,
        help_text = 'A1.07')

    def __str__(self):
        return f'{self.nama_matkul}'