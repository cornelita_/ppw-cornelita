from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import MataKuliah
from .forms import MataKuliahForm

# Create your views here.
def mata_kuliah(request):
    if (request.method == 'POST'):
        MataKuliah.objects.get(id = request.POST['id']).delete()

    mataKuliah = MataKuliah.objects.all()
    html = 'pages/mata-kuliah.html'
    return render(request, html, {'mata_kuliah': mataKuliah})

def detail_matkul(request, id):
    if (request.method == 'POST'):
        MataKuliah.objects.get(id = request.POST['id']).delete()
        mataKuliah = MataKuliah.objects.all()
        html = 'pages/mata-kuliah.html'
        return render(request, html, {'mata_kuliah': mataKuliah})

    mataKuliah = MataKuliah.objects.get(pk = id)
    html = 'pages/detail-matkul.html'
    return render(request, html, {'mata_kuliah': mataKuliah})

def form_matkul(request):
    form = MataKuliahForm(request.POST or None)
    html = 'pages/form-matkul.html'
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/list-matkul')
    else:
        return render(request, html, {'form': form})
