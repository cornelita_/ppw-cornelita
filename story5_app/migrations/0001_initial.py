# Generated by Django 3.1.1 on 2020-10-14 10:45

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MataKuliah',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_matkul', models.CharField(help_text='PPW', max_length=100, null=True, verbose_name='Mata Kuliah')),
                ('dosen_pengajar', models.CharField(help_text='Gladhi Guarddin S.Kom., M. Kom.', max_length=100, null=True, verbose_name='Dosen Pengajar')),
                ('jumlah_sks', models.IntegerField(default=1, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(24)], verbose_name='Jumlah SKS')),
                ('deskripsi', models.CharField(max_length=125, null=True, verbose_name='Deskripsi')),
                ('semester_tahun', models.CharField(choices=[('Gasal 2020/2021', 'Gasal 2020/2021'), ('Genap 2019/2020', 'Genap 2019/2020'), ('Gasal 2019/2020', 'Gasal 2019/2020'), ('Other', 'Other')], default='Gasal 2020/2021', max_length=15, verbose_name='Semester Tahun')),
                ('ruang_kelas', models.CharField(blank=True, help_text='A1.07', max_length=20, verbose_name='Ruang Kelas')),
            ],
        ),
    ]
