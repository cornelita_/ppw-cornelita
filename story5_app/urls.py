from django.urls import path
from . import views

app_name = 'story5_app'
urlpatterns = [
    path('add', views.form_matkul, name="form_url"),
    path('detail/<int:id>', views.detail_matkul, name="detail_url"),
    path('list-matkul', views.mata_kuliah, name="listmatkul_url"),
]